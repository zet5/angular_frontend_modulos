export class MenuItem {
    constructor(
        public id: number,
        public parentId: number,
        public menuId: number,
        public authItem: number,
        public label: string,
        public icon: string,
        public url: string,
        public descripcion: string,
        public orden: string,
        public isVisible: boolean,
        public idEstatus: number
    ) {
    }
}
