export class TimeZone {
    constructor(
        public id: string = null,
        public timezone: string = null,
        public offset: string = null
    ) {}
}
