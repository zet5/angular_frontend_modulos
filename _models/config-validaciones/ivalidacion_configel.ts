import { IValidacionRegla } from './ivalidacion_regla';
export interface IValidacionConfigElemento {
    nombre: string;
    referencia: any;
    reglas: Array<IValidacionRegla>;
    reglasAsync?: Array<IValidacionRegla>;
}
