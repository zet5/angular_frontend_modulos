import { Escenarios } from './escenarios';
import { IValidacionConfigElemento } from './ivalidacion_configel';
import { IValidacionRegla } from './ivalidacion_regla';

export { Escenarios, IValidacionConfigElemento, IValidacionRegla };
