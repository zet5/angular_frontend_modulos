export class Estatus {
    constructor(
        public id: number,
        public nombre: string,
        public abrev: string,
        public proceso: string,
        public estatus: string
    ) {
    }
}
