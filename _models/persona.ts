export class Persona {
    constructor(
        public id: number = null,
        public codigoIdentidad: string = null,
        public tipoCodigoIdentidad: string = null,
        public nombre: string = null,
        public apPaterno: string = null,
        public apMaterno: string = null,
        public nombreCompleto: string = null,
        public fechaNacimiento: string = null,
        public sexo: string = null
    ) { }
}
