export enum EstatusMap {
    ACTIVO = 1,
    INACTIVO = 2,
    TRANSMITIENDO = 3,
    SINTRANSMITIR = 4,
    SOLICITADA = 5,
    ACEPTADA = 6,
    RECHAZADA = 7
}
