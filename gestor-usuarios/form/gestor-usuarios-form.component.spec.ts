import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestorUsuariosFormComponent } from './gestor-usuarios-form.component';

describe('GestorUsuariosFormComponent', () => {
  let component: GestorUsuariosFormComponent;
  let fixture: ComponentFixture<GestorUsuariosFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestorUsuariosFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestorUsuariosFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
