import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { Location } from '@angular/common';
import { DateAdapter, NativeDateAdapter, MatSelectionList } from '@angular/material';
import { FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';

import { Usuario } from '../_models/usuario';
import { UsuarioService } from '../_services/usuario.service';
import { Genero } from '../../_models/genero';
import { GeneroService } from '../../_services/genero.service';
import { Estatus } from '../../_models/estatus';
import { EstatusService } from '../../_services/estatus.service';
import { ElementoAuth } from '../../rbac/_models/elemento-auth';
import { ElementoAuthService } from '../../rbac/_services/elemento-auth.service';

import { Globals } from '../../globals';
import { IValidacionConfigElemento, Escenarios } from '../../_models/config-validaciones';
import { AppComponent } from '../../app.component';
import { TimeZoneService } from '../../_services/time-zone.service';
import { TimeZone } from '../../_models/time-zone';
import { Subject } from 'rxjs/Subject';
import { LoginService } from '../../login/_services/login.service';
import { PermisosMap } from '../../_models/permisos-map';
import { Subscription } from 'rxjs/Subscription';
import { MessagesSystemComponent } from '../../messages-system/messages-system.component';

@Component({
  selector: 'app-gestor-usuarios-form',
  templateUrl: './gestor-usuarios-form.component.html',
  styleUrls: ['./gestor-usuarios-form.component.scss']
})

export class GestorUsuariosFormComponent implements OnInit {
  private frmUser: FormGroup;
  private configsValidacion: Array<IValidacionConfigElemento> = [];
  /**
   * variables para el control de bandera de isLoading
   */
  private isLoading = false;
  private textIsLoading = 'Cargando...';

  private objUsuario: Usuario;
  // Ocultar el password
  private hide: Boolean = true;
  private maxDate = new Date();
  private step = -1;

  private tiposIdentidad: object[] = [
    { id: 'CURP', tipo: 'CURP' },
    { id: 'RFC', tipo: 'RFC' },
    { id: 'LICENCIA', tipo: 'LICENCIA' },
    { id: 'CREDENCIAL DE ELECTOR', tipo: 'CREDENCIAL DE ELECTOR' },
    { id: 'OTROS', tipo: 'OTROS' }
  ];
  private sexos: Observable<Genero[]>;
  private estatus: Observable<Estatus[]>;

  //#region Variables para la seccion de permisos
  private showOpe = true;
  @ViewChild('lstOperaciones') private lstOperaciones: MatSelectionList;
  private operaciones: Observable<ElementoAuth[]>;
  private showTar = true;
  @ViewChild('lstTareas') private lstTareas: MatSelectionList;
  private tareas: Observable<ElementoAuth[]>;
  private showRol = true;
  @ViewChild('lstRoles') private lstRoles: MatSelectionList;
  private roles: Observable<ElementoAuth[]>;
  //#endregion

  constructor(
    public route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private dateAdapter: DateAdapter<NativeDateAdapter>,
    private objUsuarioService: UsuarioService,
    private objElementoAuthService: ElementoAuthService,
    private objGeneroService: GeneroService,
    private objEstatusService: EstatusService,
    private objTimeZoneService: TimeZoneService,
    private objLoginService: LoginService,
    private objMessagesSystem: MessagesSystemComponent,
    private objGlobals: Globals
  ) {
    this.objUsuario = new Usuario();
    this.setConfigValidacion();
    dateAdapter.setLocale('us-EN');
  }

  private setConfigValidacion() {
    this.configsValidacion = [
      {
        nombre: 'username',
        referencia: this.objUsuario.username,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.minLength(4)
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(75)
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.email
          }
        ],
        reglasAsync: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: this.objGlobals.debounce(this.validateEmail.bind(this))
          }
        ]
      },
      {
        nombre: 'password',
        referencia: this.objUsuario.password,
        reglas: [
          {
            escenario: [Escenarios.INSERT],
            validacion: Validators.required,
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.minLength(4)
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(50)
          }
        ]
      },
      {
        nombre: 'idZonaHoraria',
        referencia: this.objUsuario.tz,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required,
          }
        ]
      },
      {
        nombre: 'idEstatus',
        referencia: this.objUsuario.idEstatus,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required,
          }
        ]
      },
      {
        nombre: 'codigoIdentidad',
        referencia: this.objUsuario.datosPersonales.codigoIdentidad,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.minLength(18),
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(25)
          }
        ]
      },
      {
        nombre: 'tipoCodigoIdentidad',
        referencia: this.objUsuario.datosPersonales.tipoCodigoIdentidad,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.minLength(1),
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(21)
          }
        ]
      },
      {
        nombre: 'nombre',
        referencia: this.objUsuario.datosPersonales.nombre,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required,
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: this.objGlobals.getValidatorForNames()
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.minLength(3),
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(100)
          }
        ]
      },
      {
        nombre: 'apPaterno',
        referencia: this.objUsuario.datosPersonales.apPaterno,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required,
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.minLength(3),
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(50)
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: this.objGlobals.getValidatorForNames()
          }
        ]
      },
      {
        nombre: 'apMaterno',
        referencia: this.objUsuario.datosPersonales.apMaterno,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.minLength(3),
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(50)
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: this.objGlobals.getValidatorForNames()
          }
        ]
      },
      {
        nombre: 'fechaNacimiento',
        referencia: this.objUsuario.datosPersonales.fechaNacimiento,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required,
          }
        ]
      },
      {
        nombre: 'sexo',
        referencia: this.objUsuario.datosPersonales.sexo,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required,
          }
        ]
      }
    ];
  }

  ngOnInit() {
    this.frmUser = this.objGlobals.preConfigurarValidadores(this.configsValidacion);
    this.getUsuarioFromQueryParams();
    this.sexos = this.objGeneroService.getGeneros();
    this.estatus = this.objEstatusService.getEstatus();
  }

  private loadPermisos(): void {
    this.operaciones = this.objElementoAuthService.getElementosAuth({ id: 'O', nombre: 'Operacion' });
    this.tareas = this.objElementoAuthService.getElementosAuth({ id: 'T', nombre: 'Tarea' });
    this.roles = this.objElementoAuthService.getElementosAuth({ id: 'R', nombre: 'Rol' });
  }

  private getUsuarioFromQueryParams(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        if (params.get('id') === null || isNaN(+params.get('id'))) {
          this.step = 0;
          /**
           * Verificar si el objeto proviene del formulario de solicitudes
           */
          if (params.get('objPreUsuario') !== null) {
            this.isLoading = true;
            const preObjUsuario = <Usuario> JSON.parse(params.get('objPreUsuario'));
            setTimeout(() => {
              this.objUsuario.username = preObjUsuario.username;
              this.isLoading = false;
            }, 500);
          }
          this.objGlobals.posConfigurarValidadores(this.frmUser, this.configsValidacion, Escenarios.INSERT);
          this.loadPermisos();
        } else {
          // Si se cuenta con ID de trata de Edicion de un Usuario
          this.isLoading = true;
          this.objUsuarioService.getUsuario(+params.get('id')).subscribe((resp) => {
            this.objUsuario = <Usuario> resp;
            this.frmUser.get('idZonaHoraria').setValue(this.objUsuario.objTz);
            this.objGlobals.posConfigurarValidadores(this.frmUser, this.configsValidacion, Escenarios.UPDATE);
            this.loadPermisos();
            this.isLoading = false;
          });
        }
        return Observable.empty();
      }).subscribe();
  }

  private getErrorMessage(elemento: any): string {
    return this.objGlobals.getErrorMessage(this.frmUser, elemento);
  }

  private onClickBtnSave(): any {
    if (!this.frmUser.valid) {
      this.objMessagesSystem.openSnackBar('Formulario no valido');
      return false;
    }
    this.isLoading = true;
    this.textIsLoading = 'Guardando...';
    this.recolectarPermisos();

    this.objUsuarioService
      .saveUsuario(this.objUsuario)
      .subscribe(response => {
        this.isLoading = false;
        this.objMessagesSystem.openSnackBar('Guardado con éxito');
        if (this.objLoginService.checkAccess(PermisosMap.CLIENTE)) {
          this.location.back();
        } else if (this.objLoginService.checkAccess(PermisosMap.ADMINISTRADOR)) {
          this.router.navigate(['/usuarios']);
        }
      });
  }

  private onClickBtnAtras(): any {
    this.location.back();
  }
  /**
   * Funcion validadora para verificar en el servidor si el Email ya ha sido
   * previamente registrado
   * @param control Elemento a validar
   */
  public validateEmail(control: FormControl) {
    return this.objUsuarioService.validaEmailExist(control.value, this.objUsuario.id).map(existeMail => {
      if (existeMail) {
        return {emailExist: true};
      } else {
        return null;
      }
    });
  }

  //#region Metodos para la gestion de permisos

  /**
   * Al cargar el formulario en modo de edicion recorre los permisos hijo
   * relacionadosal elemento y de contar con algun permiso se selecciona
   * @param objElementoAutorizacion Item de Elemento autorizacion de algun matList
   */
  private isAuthSelect(objElementoAutorizacion: ElementoAuth): boolean {
    let bandExist = false;
    if (this.objUsuario.permisos != null && this.objUsuario.permisos.length > 0) {
      this.objUsuario.permisos.forEach(itemPermiso => {
        if (itemPermiso.id === objElementoAutorizacion.id) {
          bandExist = true;
        }
      });
    }
    return bandExist;
  }
   /**
   * Se encarga de leer los distintos matlist con los permisos seleccionados
   * y ponerlos todos en la propiedad hijos del objeto ElementoAutorizacion
   */
  private recolectarPermisos(): void {
    // Recolectar Operaciones Seleccionadas
    this.objUsuario.permisos = [];

    if (this.lstOperaciones !== undefined) {
      this.lstOperaciones.selectedOptions.selected.forEach(
        (itemSelect, index, arraySelection) => {
          this.objUsuario.permisos.push(<ElementoAuth>itemSelect.value);
        }
      );
    }
    // Recolectar Tareas seleccionadas
    if (this.lstTareas !== undefined) {
      this.lstTareas.selectedOptions.selected.forEach(
        (itemSelect, index, arraySelection) => {
          this.objUsuario.permisos.push(<ElementoAuth>itemSelect.value);
        }
      );
    }
    // Recolectar Roles seleccionados
    if (this.lstRoles !== undefined) {
      this.lstRoles.selectedOptions.selected.forEach(
        (itemSelect, index, arraySelection) => {
          this.objUsuario.permisos.push(<ElementoAuth>itemSelect.value);
        }
      );
    }
  }
  //#endregion

  //#region Funciones para autocompletado
  //#region Autocompletado de Rastreables
  /**
   * Funcion que el Componente utiliza para realizar la busqueda
   * cada vez que se tipee algo sobre el
   * @param search Texto a buscar
   */
  private fnSearchTzs(search: Observable<string>): any {
    return this.objTimeZoneService.search(search);
  }
  /**
   * Evento al momento de seleccionar una zona horaria para el usuario
   * @param itemTzSelect rastreable seleccionado
   */
  private onSelectTz(itemTzSelect: TimeZone): void {
    let setValue = null;
    if (itemTzSelect !== undefined && itemTzSelect !== null) {
      setValue = itemTzSelect.id;
    }
    this.frmUser.get('idZonaHoraria').setValue(setValue);
    this.objUsuario.tz = setValue;
  }
  /**
   * Funcion que el Componente utiliza para obtener el texto
   * que sera visible del objeto seleccionado
   * @param objTzItem Cada elemento a ser renderizado tanto en la lista
   * de sugerencias como al momento de haber seleccionado algun
   */
  private getTextObjTzItem(objTzItem: TimeZone): string {
    if (
      objTzItem !== undefined &&
      objTzItem != null &&
      objTzItem.timezone !== null &&
      objTzItem.offset !== undefined
    ) {
      return objTzItem.timezone + ' [' + objTzItem.offset + ']';
    }
    return '';
  }
  //#endregion
  //#endregion

  //#region Funciones varias

  private checkAccessGestionPermisos(): boolean {
    return this.checkAccess(PermisosMap.AUTORIZACIONES_GESTION);
  }
  public checkAccess(idPermiso: number): boolean {
    return this.objLoginService.checkAccess(idPermiso);
  }
  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
  //#endregion
}
