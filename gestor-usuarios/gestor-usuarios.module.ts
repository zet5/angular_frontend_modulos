import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms';

import { GestorUsuariosRoutingModule } from './gestor-usuarios-routing.module';
import { MaterialModule } from '../material.module';

import { UsuarioService } from './_services/usuario.service';

import { GestorUsuariosHomeComponent } from './home/gestor-usuarios-home.component';
import { GestorUsuariosListaComponent } from './lista/gestor-usuarios-lista.component';
import { GestorUsuariosFormComponent } from './form/gestor-usuarios-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GestorUsuariosRoutingModule,
    MaterialModule
  ],
  declarations: [
    GestorUsuariosHomeComponent,
    GestorUsuariosListaComponent, GestorUsuariosFormComponent
  ],
  providers: [ UsuarioService ]
})
export class GestorUsuariosModule { }
