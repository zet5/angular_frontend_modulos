import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Usuario } from '../_models/usuario';
import { Persona } from '../../_models/persona';
import { Globals  } from '../../globals';

@Injectable()
export class UsuarioService {
  private URL_API: string;
  public constructor(
    private http: HttpClient,
    private objGlobal: Globals
  ) {
    this.URL_API = this.objGlobal.BASE_URL + 'usuarios';
  }
   /**
   * Busca Usuarios en base a un termino de busqueda
   * @param query Termino de busqueda a aplicar
   */
  public search(query: Observable<string>): Observable<Usuario[]> {
    return query
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(term => this.getUsuarios(term));
  }
  /**
   * Valida si en el servidor ya existe una cuenta de correo electronico
   * @param email Email a validar
   * @param idUsuario IdUsuario en caso de que se editando
   */
  public validaEmailExist(email: string, idUsuario?: number): Observable<boolean> {
    let params = new HttpParams()
      .set('email', email + '');

    /**
     * en caso de editar no cuenta al mismo usuario en la busqueda
     */
    if (idUsuario !== undefined && idUsuario != null) {
        params = params.set('idUsuario', idUsuario + '');
    }
    return this.http.get(
        (this.URL_API + '/validaEmailExist'),
        {params: params}
    )
    .map(resp => {
      return <boolean>resp;
    });
  }
  public getUsuarios(query?: string): Observable<Usuario[]> {
    let params = new HttpParams();
    if (query !== undefined && query != null) {
      params = params.set('query', query + '');
    }
    return this.http.get(
      (this.URL_API),
      { params }
    )
    .map(this.fnOnGetUsuariosSucccess.bind(this))
    .catch((error: any) => Observable.throw(error || 'Server error'));
  }
  public getUsuario(id: number | string): Observable<Usuario> {
    const params = new HttpParams()
      .set('id', id + '');
    return this.http.get(
        (this.URL_API),
        {params: params}
    )
    .map(this.fnOnGetUsuarioSucccess.bind(this));
  }
  public saveUsuario(objUsuario: Usuario): Observable<number | any> {
    return (objUsuario.id != null) ? this.updateUsuario(objUsuario) : this.addUsuario(objUsuario);
  }
  // Retorna el id del usuario
  private addUsuario(objUsuario: Usuario): Observable<number | any> {
    return this.http.post(this.URL_API, {objUsuario: objUsuario})
      .map((response: Usuario) => {
        return response.id;
      });
  }
  private updateUsuario(objUsuario: Usuario): Observable<number | any> {
    return this.http.put(this.URL_API, {objUsuario: objUsuario})
      .map((response: Usuario) => {
        return response.id;
      });
  }
  private fnOnGetUsuariosSucccess(response): Usuario[] {
    return response.map((objUsuario: Usuario) => {
      return objUsuario;
    });
  }
  private fnOnGetUsuarioSucccess(response): Usuario {
    return <Usuario> response;
  }
}
