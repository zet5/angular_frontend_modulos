import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GestorUsuariosHomeComponent } from './home/gestor-usuarios-home.component';
import { GestorUsuariosListaComponent } from './lista/gestor-usuarios-lista.component';
import { GestorUsuariosFormComponent } from './form/gestor-usuarios-form.component';
import { GuardianGuard } from '../_services/guardian.guard';
import { PermisosMap } from '../_models/permisos-map';

const gestorUsuariosRoutes: Routes = [
  // { path: '/usuarios', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'usuarios',  component: GestorUsuariosHomeComponent,
    canActivate: [GuardianGuard],
    data: {idPermiso: PermisosMap.USUARIOS_LISTAR}
  },
  {
    path: 'usuarios/:id', component: GestorUsuariosFormComponent,
    canActivate: [GuardianGuard],
    data: {idPermiso: PermisosMap.USUARIOS_EDITAR}
  },
  {
    path: 'usuarios/add/:objPreUsuario', component: GestorUsuariosFormComponent,
    canActivate: [GuardianGuard],
    data: {idPermiso: PermisosMap.USUARIOS_CREAR}
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(gestorUsuariosRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class GestorUsuariosRoutingModule { }

