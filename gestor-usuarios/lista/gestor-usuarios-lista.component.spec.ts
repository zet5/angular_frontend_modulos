import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestorUsuariosListaComponent } from './gestor-usuarios-lista.component';

describe('GestorUsuariosListaComponent', () => {
  let component: GestorUsuariosListaComponent;
  let fixture: ComponentFixture<GestorUsuariosListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestorUsuariosListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestorUsuariosListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
