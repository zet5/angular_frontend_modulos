import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';

import { Router, ActivatedRoute } from '@angular/router';

import { Usuario } from '../_models/usuario';
import { UsuarioService } from '../_services/usuario.service';

import { ToolbarService } from '../../toolbar/_services/toolbar.service';
import { LoginService } from '../../login/_services/login.service';
import { PermisosMap } from '../../_models/permisos-map';
import { LoginInterceptor } from '../../login/_services/login-interceptor';

@Component({
  selector: 'app-gestor-usuarios-lista',
  templateUrl: './gestor-usuarios-lista.component.html',
  styleUrls: ['./gestor-usuarios-lista.component.scss']
})
export class GestorUsuariosListaComponent implements OnInit, AfterViewInit, OnDestroy {
  public usuarios: Observable<Usuario[]>;
  /**
   * variables para el control de bandera de isLoading
   */
  private isLoading = true;
  private subscriptionBandLoading: Subscription;

   // Bandera para indicar que la lista esta vacia
   private isEmpty = false;

  public searchTerm: Subject<string> = new Subject<string>();
  public subscriptionChangeSearch: Subscription = null;

  constructor(
    private service: UsuarioService,
    private tbs: ToolbarService,
    private router: Router,
    private objLoginService: LoginService,
    private objInterceptor: LoginInterceptor
  ) {
    this.inicializarToolbarSearch();
  }

  private inicializarToolbarSearch(): void {
    // Notificar al toolbar que muestre el campo de busqueda
    this.tbs.fireBandIsVisibleFieldSearch(true);
    // Suscribir al evento change del campo de busqueda de la barra
    this.subscriptionChangeSearch = this.tbs.getBandKeyPressOnTxtSearch().subscribe(this.onSearchFieldChange.bind(this));
    // Inicializar la busqueda
    this.service.search(this.searchTerm).subscribe((resp: Array<Usuario>) => {
      if (resp.length === 0) {
        this.isEmpty = true;
      } else {
        this.isEmpty = false;
      }
      this.usuarios = Observable.of(resp);
    });
  }

  ngOnInit() {
    // Se muestran todos
    this.searchTerm.next();
  }

  ngAfterViewInit() {
    /**
     * Suscribir a las notificaciones del interceptor de comunicaciones
     * para la bandera de isLoading
     */
    this.subscriptionBandLoading = this.objInterceptor
      .subscriptionBandLoading().subscribe(this.fnConstrolIsLoading.bind(this));
  }

  ngOnDestroy() {
    this.tbs.fireBandIsVisibleFieldSearch(false);
    this.subscriptionChangeSearch.unsubscribe();
    this.subscriptionBandLoading.unsubscribe();
  }

  public onClickAddusuario(): void {
    this.router.navigate(['/usuarios/add']);
  }

  public onClickListItem(itemUsuarioSelect: Usuario): void | boolean {
    if (!this.checkAccess(PermisosMap.USUARIOS_EDITAR)) {
      return false;
    }
    this.router.navigate(['/usuarios', itemUsuarioSelect.id]);
  }
  private checkAccessfbOnList(): boolean {
    return this.checkAccess(PermisosMap.USUARIOS_CREAR);
  }
  public checkAccess(idPermiso: number): boolean {
    return this.objLoginService.checkAccess(idPermiso);
  }
  /**
   * Funcion subscrita al servicio de tollbarrservice para realizar el filtrado
   * de la lista de Usuarios en base a lo que se escriba en el
   * campo de busqueda ubicado en el elemento toolbar.
   * @param query Texto a filtrar en el listado de Usuarios
   */
  public onSearchFieldChange(query: string): void {
    this.searchTerm.next(query);
  }
  /**
   * Controla la bandera de estatus del loading del interceptor de
   * comunicaciones
   * @param loading Bandera que indica si se esta cargando un recurso
   */
  private fnConstrolIsLoading (loading: boolean): void {
    if (this.isLoading !== loading) {
      this.isLoading = loading;
    }
  }
}
