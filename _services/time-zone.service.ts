import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { TimeZone } from '../_models/time-zone';
import { Globals } from '../globals';

@Injectable()
export class TimeZoneService {
  private URL_API: string;
  public constructor(
    private http: HttpClient,
    private objGlobal: Globals
  ) {
    this.URL_API = this.objGlobal.BASE_URL + 'TimeZones';
  }

  /**
   * Busca elementos Zonas Horarias en base a un termino de busqueda
   * @param query Termino de busqueda a aplicar
   */
  public search(query: Observable<string>): Observable<TimeZone[]> {
    return query
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(term => this.getTimeZones(term));
  }

  public getTimeZones(query?: string): Observable<TimeZone[]> {
    let params = new HttpParams();
    if (query !== undefined && query != null) {
      params = params.set('query', query + '');
    }
    return this.http.get(
      (this.URL_API),
      { params }
    )
    .map(this.fnOnGetTimeZonesSucccess.bind(this))
    .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  private fnOnGetTimeZonesSucccess(response): TimeZone[] {
    return response.map((objTimeZone: TimeZone) => {
      return <TimeZone>objTimeZone;
    });
  }
}
