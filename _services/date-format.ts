import { NativeDateAdapter } from '@angular/material';

export class DateFormat extends NativeDateAdapter {
    format(date: any, displayFormat: Object): string {
        const day = date.getUTCDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        return `${day}/${month}/${year}`;
    }
}
