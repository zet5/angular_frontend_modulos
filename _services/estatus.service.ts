import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Estatus } from '../_models/estatus';
import { Globals } from '../globals';

@Injectable()
export class EstatusService {
  private URL_API: string;
  public constructor(
    private http: HttpClient,
    private objGlobal: Globals
  ) {
    this.URL_API = this.objGlobal.BASE_URL + 'estatus';
  }

  public getEstatus(): Observable<Estatus[]> {
    const params = new HttpParams();
    return this.http.get(
      (this.URL_API),
      { params }
    )
    .map(this.fnOnGetEstatusSucccess.bind(this))
    .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  private fnOnGetEstatusSucccess(response): Estatus[] {
    return response.map((objEstatus: Estatus) => {
      return <Estatus>objEstatus;
    });
  }
}
