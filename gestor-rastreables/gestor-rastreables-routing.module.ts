import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ListaComponent } from './lista/lista.component';
import { FormComponent } from './form/form.component';
import { GuardianGuard } from '../_services/guardian.guard';
import { PermisosMap } from '../_models/permisos-map';

const gestorRastreablesRoutes: Routes = [
  {
    path: 'rastreables',  component: HomeComponent,
    canActivate: [GuardianGuard],
    data: {idPermiso: PermisosMap.RASTREABLES_LISTAR}
  },
  {
    path: 'rastreables/:id', component: FormComponent,
    canActivate: [GuardianGuard],
    data: {idPermiso: PermisosMap.RASTREABLES_EDITAR}
  },
  {
    path: 'rastreables/add', component: FormComponent,
    canActivate: [GuardianGuard],
    data: {idPermiso: PermisosMap.RASTREABLES_CREAR}
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(gestorRastreablesRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class GestorRastreablesRoutingModule { }

